<div class="panel-container panel-flex panel-search">

    <?php if(isset($this->session->userdata['logged_in'])) redirect('admin/dashboard'); ?>

    <div class="form-container">

        <?php if($this->session->flashdata('message') != '') ?>
            <span class="message"><?php echo $this->session->flashdata('message'); ?></span>
        <?php  ?>

        <span class="message"><?php echo validation_errors(); ?></span>

        <h1>Dashboard - Login</h1>

        <?php echo form_open('admin/login'); ?>

            <label>Username :</label>
            <input type="text" name="username" value="<?php echo set_value('username'); ?>" />

            <label>Password :</label>
            <input type="password" name="password" value="<?php echo set_value('password'); ?>" />

            <div class="submit-container">
                <button type="submit" value=" Login " name="submit">Login!</button>
                <a href="<?php echo base_url() ?>"><i class="fa fa-search" aria-hidden="true"></i> Start your search</a>
                <a href="<?php echo base_url() ?>admin/register"><i class="fa fa-user-plus" aria-hidden="true"></i> Setup new Admin Account</a>
            </div>

        <?php echo form_close(); ?>

    </div>

</div>
