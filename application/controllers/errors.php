<?php

class Errors extends CI_Controller {

    public function index() {
        parent::__construct();
    }

    public function emptysearch() {
        $data = array('title' => 'MovieDB - Empty Search Term');
        $this->load->view('templates/header', $data);
        $this->load->view('errors/emptysearch');
        $this->load->view('templates/footer');
    }
}
