<?php
class Admin extends CI_Controller {

    public function __constructor() {
        parent::__constructor();
    }

    public function index() {

        $data = array('title' => 'MovieDB - Admin');

        $this->load->view('templates/admin/header', $data);
        $this->load->view('admin/login');
        $this->load->view('templates/admin/footer');
    }

    public function register() {

        if(isset($this->session->userdata['logged_in'])) :

            $data = array('title' => 'MovieDB - Admin Dashboard');
            $this->load->view('templates/admin/header', $data);
            $this->load->view('admin/dashboard');
            $this->load->view('templates/admin/footer');

        else :

            $data = array('title' => 'MovieDB - Admin Register');
            $this->load->view('templates/admin/header', $data);
            $this->load->view('admin/register');
            $this->load->view('templates/admin/footer');

        endif;
    }

    public function register_setup() {

        $this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[12]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if($this->form_validation->run() == false) :
            
            redirect('admin/register');

        else :

            $password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);

            $data = array(
                'username' => $this->input->post('username'),
                'password' => $password
            );

            $result = $this->admin_login->go_register($data);

            if($result == true) :

                $username = $this->input->post('username');
                $result = $this->admin_login->go_register($username);

                $session_data = array(
                    'username' => $result[0]->user_name,
                );

                $this->session->set_userdata('logged_in', $session_data);

                redirect('admin/dashboard');

            else :

                redirect('admin/register');

            endif;

        endif;

    }

    public function login() {

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

        if($this->form_validation->run() == FALSE) :

            if(isset($this->session->userdata['logged_in'])) :

                $data = array('title' => 'MovieDB - Admin Dashboard');
                $this->load->view('templates/admin/header', $data);
                $this->load->view('admin/dashboard');
                $this->load->view('templates/admin/footer');

            else :

                $data = array('title' => 'MovieDB - Admin Login');
                $this->load->view('templates/admin/header', $data);
                $this->load->view('admin/login');
                $this->load->view('templates/admin/footer');

            endif;

        else :

            $get_user = $this->input->post('username');
            $get_pass = $this->input->post('password');

            $data = array(
                'username' => $get_user,
                'password' => $get_pass
            );

            $result = $this->admin_login->go($data);

            if($result == true) :

                $username = $this->input->post('username');
                $result = $this->admin_login->go($username);

                $session_data = array(
                    'username' => $result[0]->user_name,
                );

                $this->session->set_userdata('logged_in', $session_data);

                redirect('admin/dashboard');

            else : 

                $this->session->set_flashdata('message', 'Sorry, your login details incorrect!');
                redirect('admin/login');

            endif;

        endif;

    }

    public function logout() {

        $sess_array = array(
            'username' => ''
        );

        $this->session->unset_userdata('logged_in', $sess_array);
        $this->session->sess_destroy();
        $data = array('message' => 'Successfully Logged Out!', 'title' => 'MediaDB - Logged Out!');

        $this->load->view('templates/admin/header', $data);
        $this->load->view('admin/login', $data);
        $this->load->view('templates/admin/footer');

    }

    public function dashboard() {

        $data = array('title' => 'MovieDB - Admin Dashboard');
        $this->load->view('templates/admin/header', $data);
        $this->load->view('admin/dashboard');
        $this->load->view('templates/admin/footer');

    }
}
