<?php

class Search extends CI_Controller {

    public function index() {
        parent::__construct();
    }

    public function results() {

        $this->form_validation->set_rules('media', 'Media', 'trim|required');

        if($this->form_validation->run() == false) :

            redirect('/');

        else:

            $term = $this->input->post('media');

            // $this->db->select('*')->from('mdb_media_data')->group_start()->like('media_name', $term)->group_end();

            if($this->config->item('enable_moviedb_api') == true) :

                $api = $this->config->item('moviedb_api_v3');// Set config options (only 'server' is required to work)

                $config = array('server' 			=> 'https://api.themoviedb.org/3/search/movie',
                				'api_key'			=> $api
                				);

                // Run some setup
                $this->rest->initialize($config);

                $client = new http\Client;
                $request = new http\Client\Request;

                $body = new http\Message\Body;
                $body->append('{}');

                $request->setRequestUrl('');
                $request->setRequestMethod('GET');
                $request->setBody($body);

                $request->setQuery(new http\QueryString(array(
                  'include_adult' => 'false',
                  'page' => '1',
                  'language' => 'en-US',
                  'api_key' => $api
                )));

                $client->enqueue($request)->send();
                $response = $client->getResponse();

                echo $response->getBody();
            endif;

            $data = array(
                'title' => 'Search Results for ' . $term,
                'results' => $response
            );

            $this->load->view('templates/header', $data);
            $this->load->view('content/search-results', $data);
            $this->load->view('templates/footer');

        endif;
    }
}
