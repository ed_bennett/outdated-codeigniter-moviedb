<?php

class Admin_Login extends CI_Model {

    public function go($data) {

        $check = "username ='" . $data['username'] . "'";

        $this->db->select('*');
        $this->db->from('mdb_users');
        $this->db->where($check);
        $this->db->limit(1);

        $query = $this->db->get();
        $pass = $query->result_array();
 
        if(empty($pass) ) :

            $data = array(
                'message' => 'No user found matching these details!',
                'title' => 'MediaDB - Admin Login'
            );

            $this->load->view('templates/admin/header', $data);
            $this->load->view('admin/login');
            $this->load->view('templates/admin/footer');
           
        else :
            
            if(password_verify($data['password'], $pass[0]['admin_pass']) == true) : 

                return true; 

            else : 

                $data = array(
                    'message' => 'Your password is incorrect!',
                    'title' => 'MediaDB - Admin Login'
                );

                $this->load->view('templates/admin/header', $data);
                $this->load->view('admin/login');
                $this->load->view('templates/admin/footer');

                return false; 

            endif;

        endif;

    }

    public function go_register($data) {

        $check = "username='" . $data['username'] . "'";
        
        $this->db->select('*');
        $this->db->from('mdb_users');
        $this->db->where($check);
        $this->db->limit(1);

        $get = $this->db->get();

        if($get->num_rows() == 0) :

            $insert = array(
                'username' => $data['username'],
                'password' => $data['password']
            );

            $this->db->insert('mdb_users', $insert);

            if($this->db->affected_rows() > 0) : return true; else: return false; endif;

        endif;
    }
}
